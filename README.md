# Totem Spotify

## Install

To install the required NPM modules for both client and server side, run the 
following command from project's root folder:

```
git clone git@bitbucket.org:vmanchev-santech/totem-react.git
cd totem-react
npm run totem:install
```

## Run the project

To build the client and start the backend server, run the following command:

```
npm run totem:start
```

Open your browser on `http://localhost:3000` and perform an artist search.

## Configuration

### Backend server configuration

There is a custom Spotify module available for the backend server. It relays on 
**client_credentials** authentication option. The base clientId and clientSecret
can be found at `server/config.js` file. Feel free to replace these with your own.

## Unit tests
To run the tests, use `npm test` and find the code coverage at `./coverage/lcov-report/index.html`.
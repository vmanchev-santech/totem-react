import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, Link, browserHistory } from 'react-router';

import App from './components/App.jsx';

import NotFoundPage from './components/NotFoundPage.jsx';
import ArtistSearchPage from './components/ArtistSearchPage.jsx';
import ArtistAlbumsPage from './components/ArtistAlbumsPage.jsx';
import AlbumPage from './components/AlbumPage.jsx';

render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={ArtistSearchPage} />
      <Route path="artist-albums/:id" component={ArtistAlbumsPage} />
      <Route path="album/:id" component={AlbumPage} />
      <Route path="*" component={NotFoundPage} />
    </Route>
  </Router>
), document.getElementById('root'));

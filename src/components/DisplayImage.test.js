import React from 'react';
import DisplayImage from './DisplayImage.jsx';
import renderer from 'react-test-renderer';

let album = {
    name: 'test4',
    images: [
        { url: 'http://img.google.com/1.png', width: 130, height: 110 },
        { url: 'http://img.google.com/2.png', width: 330, height: 320 },
        { url: 'http://img.google.com/3.png', width: 630, height: 630 }
    ],
};

describe('DisplayImage', () => {

    it('should create the component', () => {
        const component = renderer.create(<DisplayImage imageList={album.images} imageType={'thumbnail'} altText={album.name} />);
        expect(component).toBeDefined();
    });

    it('should do nothing if non-existing image type was provided', () => {
        const component = renderer.create(<DisplayImage imageList={album.images} imageType={'thumbnailsssss'} altText={album.name} />);
        expect(component).toBeDefined();
        expect(component.getInstance().params.selectedImage).toEqual({});
    });

    describe('thumbnail', () => {
        it('should set selectedImage to a thumbnail', () => {

            const component = renderer.create(<DisplayImage imageList={album.images} imageType={'thumbnail'} altText={album.name} />);
            const selectedImage = component.getInstance().params.selectedImage;

            expect(selectedImage.url).toEqual('http://img.google.com/1.png');
            expect(selectedImage.width).toEqual(100);
            expect(selectedImage.height).toEqual(100);
            expect(selectedImage.altText).toEqual('test4');
        });

        it('should return default thumbnail if no images was passed', () => {
            const component = renderer.create(<DisplayImage imageList={[]} imageType={'thumbnail'} altText={album.name} />);
            const selectedImage = component.getInstance().params.selectedImage;

            expect(selectedImage.url).toEqual('http://placehold.it/100x100');
            expect(selectedImage.width).toEqual(100);
            expect(selectedImage.height).toEqual(100);
            expect(selectedImage.altText).toEqual('test4');
        })
    });

    describe('card', () => {
        it('should set selectedImage to a card', () => {
            const component = renderer.create(<DisplayImage imageList={album.images} imageType={'card'} altText={album.name} />);
            const selectedImage = component.getInstance().params.selectedImage;

            expect(selectedImage.url).toEqual('http://img.google.com/3.png');
            expect(selectedImage.width).toEqual(300);
            expect(selectedImage.height).toEqual(300);
            expect(selectedImage.altText).toEqual('test4');
        });

        it('should return default card image of no images was passed', () => {
            const component = renderer.create(<DisplayImage imageList={[]} imageType={'card'} altText={album.name} />);
            const selectedImage = component.getInstance().params.selectedImage;

            expect(selectedImage.url).toEqual('http://placehold.it/300x300');
            expect(selectedImage.width).toEqual(300);
            expect(selectedImage.height).toEqual(300);
            expect(selectedImage.altText).toEqual('test4');
        });
    });

    describe('poster', () => {
        it('should set selectedImage to poster', () => {

            const component = renderer.create(<DisplayImage imageList={album.images} imageType={'poster'} altText={album.name} />);

            const selectedImage = component.getInstance().params.selectedImage;

            expect(selectedImage.url).toEqual('http://img.google.com/2.png');
            expect(selectedImage.width).toEqual(640);
            expect(selectedImage.height).toEqual(640);
            expect(selectedImage.altText).toEqual('test4');
        });

        it('should return default poster image if no images was passed', () => {
            const component = renderer.create(<DisplayImage imageList={[]} imageType={'poster'} altText={album.name} />);

            const selectedImage = component.getInstance().params.selectedImage;

            expect(selectedImage.url).toEqual('http://placehold.it/640x640');
            expect(selectedImage.width).toEqual(640);
            expect(selectedImage.height).toEqual(640);
            expect(selectedImage.altText).toEqual('test4');
        });        
    });

});




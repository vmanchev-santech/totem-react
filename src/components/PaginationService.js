class PaginationModel {

    constructor(total, offset, limit) {
        this.total = total;
        this.offset = offset;
        this.limit = limit;

        this.totalPages = 0;
        this.currentPage = 1;
        this.prevPage;
        this.prevOffset;
        this.nextPage;
        this.nextOffset;
        this.firstVisiblePage = 1;
        this.lastVisiblePage;
        this.perPage = 20;
        this.maxPagesToDisplay = 5;
        this.pagesRange = [];

        if (this.total === 0) {
            return;
        }

        this.totalPages = Math.ceil(this.total / 20);
        this.currentPage = (this.offset === 0) ? 1 : (this.offset / this.perPage) + 1;

        this.setup();
    }

    setup() {

        if (this.total <= this.perPage) {
            return;
        }

        this.createVisiblePagesRange();

        if (this.currentPage + 1 <= this.totalPages) {
            this.nextPage = this.currentPage + 1;
            this.nextOffset = this.currentPage * this.perPage;
        } else {
            this.nextPage = null;
            this.nextOffset = this.perPage;
        }

        if (this.currentPage - 1 > 0) {
            this.prevPage = this.currentPage - 1;
            this.prevOffset = (this.prevPage - 1) * this.perPage;
        } else {
            this.prevOffset = 0;
        }

    }

    createVisiblePagesRange() {

        this.pagesRange = [];

        this.firstVisiblePage = this.currentPage - 2;

        if (this.firstVisiblePage < 1) {
            this.firstVisiblePage = 1;
        }

        this.lastVisiblePage = this.currentPage + 2;

        if (this.lastVisiblePage < this.maxPagesToDisplay) {
            this.lastVisiblePage = this.maxPagesToDisplay;
        }

        if (this.lastVisiblePage > this.totalPages) {
            this.lastVisiblePage = this.totalPages;
        }

        for (let i = this.firstVisiblePage; i <= this.lastVisiblePage; i++) {
            this.pagesRange.push(i);
        }
    }

}


export default PaginationModel;
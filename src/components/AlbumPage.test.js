import React from 'react';
import AlbumPage from './AlbumPage.jsx';
import renderer from 'react-test-renderer';
import axios from 'axios'
import moxios from 'moxios'

let mockEvent = {
    preventDefault: () => { }
};

let component = {}

describe('AlbumPage', () => {

    beforeEach(() => {
        moxios.install();
        component = renderer.create(<AlbumPage params={{ id: 222 }} />);
    });

    afterEach(function () {
        moxios.uninstall();
    });

    it('should create the component', (done) => {
        expect(component).toBeDefined();

        let request = moxios.requests.mostRecent();

        expect(request.config.url).toEqual('/api/album/222');

        request.respondWith({
            status: 200,
            response: {
                id: 222,
                name: 'test 4',
                artists: [{
                    name: 'Elvis'
                }],
                images: [{
                    url: 'image1.jpg',
                    width: 100,
                    height: 100
                }],
                tracks: {
                    items: [
                        {
                            duration_ms: 149386,
                            name: 'test 444',
                            uri: 'asdfasfasdf'
                        }
                    ]
                }
            }
        }).then(() => {
            expect(component.getInstance().state.album).toEqual({
                id: 222,
                name: 'test 4',
                artists: [{
                    name: 'Elvis'
                }],
                images: [{
                    url: 'image1.jpg',
                    width: 640,
                    height: 640,
                    altText: 'test 4'
                }],
                tracks: {
                    items: [
                        {
                            duration_ms: 149386,
                            name: 'test 444',
                            uri: 'asdfasfasdf'
                        }
                    ]
                }
            });
            done();
        });
    });

    describe('formatTrackDuration', () => {
        it('should transform track duration from milliseconds to mm:ss format', () => {
            expect(component.getInstance().formatTrackDuration(149386)).toEqual('2:29');
            expect(component.getInstance().formatTrackDuration(9238)).toEqual('0:09');
        });
    });
});
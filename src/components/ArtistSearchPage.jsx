import React from 'react';
import axios from 'axios';
import { Link } from 'react-router';
import { ScaleLoader } from 'react-spinners';

import DisplayImage from './DisplayImage.jsx';
import Pagination from './Pagination.jsx';

class ArtistSearchPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      offset: 0,
      artists: [],
      loading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(props) {

    this.doSearch = false;

    if (props.location.query && props.location.query.keyword) {
      this.state.value = props.location.query.keyword;
      this.doSearch = true;
    }

    if (props.location.query && props.location.query.offset) {
      this.state.offset = props.location.query.offset;
      this.doSearch = true;
    }

    if (this.doSearch) {
      this.handleSubmit();
    }
  }

  handleChange(event) {
    this.setState({ value: event.target.value, offset: 0 });
  }

  handleSubmit(event) {

    window.scrollTo(0,0);
    this.setState({loading: true});

    axios.get('/api/search/' + this.state.value + '?offset=' + this.state.offset)
      .then((response) => {

          this.setState({
            artists: response.data.artists.items,
            paginationParams: {
              total: response.data.artists.total,
              offset: response.data.artists.offset,
              limit: response.data.artists.limit,
              keyword: this.state.value
            },
            loading: false
          });

      });

    if (event) {
      event.preventDefault();
    }
  }

  render() {
    return (
      <div>

       {
         this.state.loading && (
          <div className='overlay'>
           <ScaleLoader color={'#337ab7'} loading={this.state.loading} />
          </div>
          )
        }

        <div className='container'>
          <div className='page-header'>
            <h1>Artistes</h1>
          </div>
          <div className='panel panel-default'>
            <div className='panel-heading'>Rechercher un artiste Spotify</div>
            <div className='panel-body'>
              <form className='form-inline' onSubmit={this.handleSubmit} onChange={this.handleChange} >
                <div className='form-group'>
                  <input type='search' value={this.state.value} className='form-control' placeholder='Mot(s)-clé(s)' />
                </div>
                <button type='submit' className='btn btn-primary'>Chercher</button>
              </form>
            </div>
          </div>
        </div>
        <div className='container artists'>
          {
            this.state.artists.map(artist =>
              <div className='row media' key={artist.id}>
                <div className='col-sm-2 media-left'>
                  <Link to={'/artist-albums/' + artist.id}>
                    <DisplayImage imageList={artist.images} imageType={'thumbnail'} altText={artist.name}>
                      {this.props.children}
                    </DisplayImage>
                  </Link>
                </div>
                <div className='col-sm-10'>
                  <Link to={'/artist-albums/' + artist.id}>
                    <h4 className='media-heading'>{artist.name}</h4>
                    {artist.genres.join(', ')}
                  </Link>
                </div>
              </div>
            )
          }

        </div>
        <Pagination params={this.state.paginationParams}>{this.props.children}</Pagination>
      </div>
    );
  }
}

export default ArtistSearchPage;

import React from 'react';
import { Link } from 'react-router';
import PaginationService from './PaginationService.js';

class Pagination extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillReceiveProps(props) {
        if(props.params){
            this.setState({keyword: props.params.keyword});
        }
        
        if (props.params) {
            this.paginationModel = new PaginationService(props.params.total, props.params.offset, props.params.limit);
        }
    }

    render() {
        return (
            <div className="container text-center">
                <nav aria-label="Page navigation">
                <ul className="pagination">
                    {this.paginationModel && this.paginationModel.prevPage > 0 && (
                        <li key="prevPage">
                            <Link to={ location.pathname + '?keyword='+(this.state.keyword ? this.state.keyword: '')+'&offset=' + this.paginationModel.prevOffset}>
                            &laquo;
                            </Link>
                        </li>
                    )}


                    {
                        this.paginationModel && (this.paginationModel.pagesRange.map(idx =>
                            (
                                <li key={idx} className={(idx === this.paginationModel.currentPage) ? 'active' : ''}>
                                    <Link to={location.pathname + '?keyword=' + (this.state.keyword ? this.state.keyword : '') + '&offset=' + ((idx - 1) * this.paginationModel.perPage)}>
                                        {idx}
                                    </Link>
                                </li>
                            )
                        ))
                    }

                    {this.paginationModel && this.paginationModel.nextPage > 0 && (
                        <li key="nextPage">
                            <Link to={ location.pathname + '?keyword='+(this.state.keyword ? this.state.keyword: '')+'&offset=' + this.paginationModel.nextOffset}>
                            &raquo;
                            </Link>
                        </li>
                    )}
                </ul>
                </nav>
            </div>
        );
    }
}


export default Pagination;
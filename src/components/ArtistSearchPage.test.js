import React from 'react';
import ArtistSearchPage from './ArtistSearchPage.jsx';
import renderer from 'react-test-renderer';
import axios from 'axios'
import moxios from 'moxios'

let mockEvent = {
    preventDefault: () => { }
};

let component = {}

describe('ArtistSearchPage', () => {

    beforeEach(() => {
        component = renderer.create(<ArtistSearchPage />);
    });

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    describe('handleChange', () => {
        it('should set keyword from event and reset the offet', () => {

            const inst = component.getInstance();

            inst.handleChange({ target: { value: 'elvis' } });

            expect(inst.state.value).toBe('elvis');
            expect(inst.state.offset).toBe(0);
        });
    });

    describe('handleSubmit', () => {

        beforeEach(() => {
            moxios.install();

            spyOn(window, 'scrollTo');

            component.getInstance().handleSubmit(mockEvent);
        });

        afterEach(function () {
            moxios.uninstall();
        });

        it('should scroll the window to top', () => {
            expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
        });

        it('should display the loading indicator', () => {
            expect(component.getInstance().state.loading).toBeTruthy();
        });

        it('should test axios', (done) => {

            let request = moxios.requests.mostRecent();

            request.respondWith({
                status: 200,
                response: {
                    artists: {
                        items: [
                            {
                                id: 1,
                                genres: ['rock', 'gospel'],
                                images: [{
                                    url: 'image1.jpg',
                                    width: 100,
                                    height: 100
                                }]
                            }
                        ]
                    },
                    total: 640,
                    limit: 20,
                    offset: 0
                }
            }).then(() => {
                expect(component.getInstance().state.artists).toEqual([{
                    id: 1,
                    genres: ['rock', 'gospel'],
                    images: [{
                        url: 'image1.jpg',
                        width: 100,
                        height: 100
                    }]
                }]);
                done();
            });
        });

        it('should prevent default action if called with event', () => {
            spyOn(mockEvent, 'preventDefault');
            component.getInstance().handleSubmit(mockEvent);
            expect(mockEvent.preventDefault).toHaveBeenCalled();
        });

        it('should not prevent default action if called without event', () => {
            spyOn(mockEvent, 'preventDefault');
            component.getInstance().handleSubmit();
            expect(mockEvent.preventDefault).not.toHaveBeenCalled();
        });

    });

    describe('componentWillReceiveProps', () => {

        beforeEach(() => {
            spyOn(component.getInstance(), 'handleSubmit');
        });

        it('should perform a search when only keyword is provided', () => {
            component.getInstance().componentWillReceiveProps({ location: { query: { keyword: 'elvis' } } });
            expect(component.getInstance().state.value).toEqual('elvis');
            expect(component.getInstance().doSearch).toBeTruthy();
        });

        it('should perform a search when only offset is provided', () => {
            component.getInstance().componentWillReceiveProps({ location: { query: { offset: 20 } } });
            expect(component.getInstance().state.offset).toEqual(20);
            expect(component.getInstance().doSearch).toBeTruthy();
        });

        it('should perform search when both keyword and offset are provided', () => {
            component.getInstance().componentWillReceiveProps({
                location: {
                    query: {
                        keyword: 'elvis',
                        offset: 20
                    }
                }
            });

            expect(component.getInstance().state.value).toEqual('elvis');
            expect(component.getInstance().state.offset).toEqual(20);
            expect(component.getInstance().doSearch).toBeTruthy();
        });

        it('should not perform a search when both keyword and offset are not provided', () => {
            component.getInstance().componentWillReceiveProps({location: {}});
            expect(component.getInstance().state.value).toEqual('');
            expect(component.getInstance().state.offset).toEqual(0);
            expect(component.getInstance().doSearch).toBeFalsy();
        });

    });

});


import React from 'react';
import axios from 'axios';
import { Link } from 'react-router';
import { ScaleLoader } from 'react-spinners';

import DisplayImage from './DisplayImage.jsx';
import Pagination from './Pagination.jsx';

class ArtistAlbumsPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: {
                items: []
            },
            id: props.params.id,
            loading: true
        };

        let searchParams = {
            id: props.params.id,
            offset: 0
        };        

        this.handleSubmit(searchParams);

    }

    handleSubmit(params) {

        window.scrollTo(0,0);

        this.state.loading = true;

        axios.get(`/api/artist/${params.id}/albums?offset=${params.offset}`)
        .then((response) => { 
            this.setState({
                data: response.data,
                paginationParams: {
                    total: response.data.total,
                    offset: response.data.offset,
                    limit: response.data.limit
                },
                loading: false
            });
        });
    }

    componentWillReceiveProps(props) {

        let searchParams = {
            id: props.params.id,
            offset: props.location.query.offset
        };
      
        this.handleSubmit(searchParams);
      }    

    render() {
        return (
            <div className='container'>

                {
                    this.state.loading && (
                        <div className='overlay'>
                            <ScaleLoader color={'#337ab7'} loading={this.state.loading} />
                        </div>
                    )
                }

                <ol className='breadcrumb'>
                    <li><Link to={'/'}>Recherche</Link></li>
                    {this.state.data.items.length > 0 && (<li className='active'>{this.state.data.items[0].artists[0].name}</li>)}
                </ol>
                <div className='page-header'>
                    <h1>Albums</h1>
                    {this.state.data.items.length > 0 && (<h2>{this.state.data.items[0].artists[0].name}</h2>)}
                </div>
                <div className='container albums'>
                    <div className='row'>
                        {
                            this.state.data.items.map(album =>
                                <div className='col-xs-12 col-sm-4 col-md-4 col-lg-3 album' key={album.id}>
                                    <div className='thumbnail text-center'>
                                        <Link to={'/album/' + album.id}>
                                            <DisplayImage imageList={album.images} imageType={'card'} altText={album.name}>
                                                {this.props.children}
                                            </DisplayImage>
                                        </Link>
                                        <div className='caption'>
                                            <h4 className="truncate">{album.name}</h4>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                <Pagination params={this.state.paginationParams}>{this.props.children}</Pagination>
            </div>
        );
    };

}

export default ArtistAlbumsPage;
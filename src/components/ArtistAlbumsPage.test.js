import React from 'react';
import ArtistAlbumsPage from './ArtistAlbumsPage';
import renderer from 'react-test-renderer';
import axios from 'axios'
import moxios from 'moxios'

let mockEvent = {
    preventDefault: () => { }
};

let component = {}
let axiosMock = {};

describe('ArtistAlbumsPage', () => {

    beforeEach(() => {
        spyOn(window, 'scrollTo');
        component = renderer.create(<ArtistAlbumsPage params={{ id: 13 }} />);
    });

    it('should create the component', () => {
        expect(component).toBeDefined();
        expect(window.scrollTo).toHaveBeenCalled();
    });

    describe('componentWillReceiveProps', () => {

        it('should prepare search params object', () => {

            spyOn(component.getInstance(), 'handleSubmit').and.callThrough();

            component.getInstance().componentWillReceiveProps({
                params: {
                    id: 101
                },
                location: {
                    query: {
                        offset: 20
                    }
                }
            });

            expect(component.getInstance().handleSubmit).toHaveBeenCalledWith({
                id: 101,
                offset: 20
            });
        });

    });
});
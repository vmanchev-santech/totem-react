import React from 'react';
import axios from 'axios';
import { ScaleLoader } from 'react-spinners';
import { Link } from 'react-router';

import DisplayImage from './DisplayImage.jsx';

class AlbumPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            album: {},
            loading: true
        };

        axios.get(`/api/album/${props.params.id}`)
            .then((response) => {
                this.setState({ 
                    album: response.data, 
                    loading: false 
                });
            })
    }

    formatTrackDuration(ms) {
        const minutes = Math.floor(ms / 60000);
        const seconds = parseInt(((ms % 60000) / 1000).toFixed(0), 10);
    
        return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;        
    }

    render() {
        return (
            <div className='container'>

                {
                    this.state.loading && (
                    <div className='overlay'>
                        <ScaleLoader color={'#337ab7'} loading={this.state.loading} />
                    </div>
                    )
                }

                <ol className='breadcrumb'>
                    <li><Link to={'/'}>Recherche</Link></li>
                    { this.state.album && this.state.album.artists && (<li><Link to={'/artist-albums/' + this.state.album.artists[0].id}>{this.state.album.artists[0].name}</Link></li>)}
                    <li className='active'>{this.state.album.name}</li>
                </ol>
                <div className='page-header'>
                    <h1>Pistes</h1>
                    { this.state.album.artists && (<h2>{this.state.album.artists[0].name} - {this.state.album.name}</h2>)}
                </div>
                <div className='row'>
                    <div className='col-xs-12 col-md-6 col-lg-6'>
                        {this.state.album.images && (
                            <DisplayImage imageList={this.state.album.images} imageType={'poster'} altText={this.state.album.name}>
                                {this.props.children}
                            </DisplayImage>
                        )}
                    </div>
                    <div className='col-xs-12 col-md-6 col-lg-6'>
                        <ul className='list-group'>
                            {
                                this.state.album.tracks && this.state.album.tracks.items.map(track => 
                                    <li key={track.uri} className='list-group-item'>{track.track_number}. {track.name} <span className='badge'>{this.formatTrackDuration(track.duration_ms)}</span></li>
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}


export default AlbumPage;
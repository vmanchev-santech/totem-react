import React from 'react';

class DisplayImage extends React.Component {

    constructor(props) {
        super(props);

        this.params = Object.assign({}, props, {selectedImage: {}});

        // build a getter method name, using the passed image type
        const method = 'get' + this.capitalizeFirstLetter(this.params.imageType);

        // call the getter and set selectedImage
        if (typeof this[method] !== 'undefined') {
            this[method]();
            this.params.selectedImage.altText = this.params.altText;
        }
    }

    capitalizeFirstLetter(word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    getThumbnail() {
        
        if (this.params.imageList.length === 0) {
    
          this.params.selectedImage = {
              url: 'http://placehold.it/100x100', 
              width: 100, 
              height: 100
          };

          return this.params.selectedImage;
        }
    
        // get the smallest image in the list
        this.params.selectedImage = this.params.imageList.reduce(function (prev, curr) {
          return prev.width < curr.width ? prev : curr;
        });
    
        this.params.selectedImage.width = 100;
        this.params.selectedImage.height = 100;
    
        return this.params.selectedImage;
    }

    getCard() {

        if (this.params.imageList.length === 0) {
    
          this.params.selectedImage = {
              url: 'http://placehold.it/300x300', 
              width: 300, 
              height: 300
          };
    
          return this.params.selectedImage;
        }
    
        // get the biggest image in the list
        this.params.selectedImage = this.params.imageList.reduce(function (prev, curr) {
          return prev.width < curr.width ? curr : prev;
        });
    
        this.params.selectedImage.width = 300;
        this.params.selectedImage.height = 300;
    
        return this.params.selectedImage;
    }

    getPoster() {

        if (this.params.imageList.length === 0) {
    
          this.params.selectedImage = {
              url: 'http://placehold.it/640x640', 
              width: 640, 
              height: 640
            };
    
          return this.params.selectedImage;
        }
    
        // get the biggest image in the list
        this.params.selectedImage = this.params.imageList.reduce(function (prev, curr) {
          return prev.width < curr.width ? curr : prev;
        });
    
        this.params.selectedImage.width = 640;
        this.params.selectedImage.height = 640;
    
        return this.params.selectedImage;
    }

    render() {
        return (
            <img 
                className={'media-object img-responsive ' + this.params.imageType}
                src={this.params.selectedImage.url}
                width={this.params.selectedImage.width}
                height={this.params.selectedImage.height}
                alt={this.params.selectedImage.altText} />
        )
    }
}

export default DisplayImage;
const request = require('request');
const fs = require('fs');
const config = require('./config');
const querystring = require('querystring');

class Spotify {
  
  constructor(config){
    this.config = config;
  }

  action(name, params, cb) {
    this.authenticate( (error, response, body) => {
      this[name](params, body.access_token, cb);
    });
  }

  authenticate (cb) {

    let options = {
      json: true,
      headers: {
        'Authorization': 'Basic ' + this.getAuthHash(),
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      form: {
        grant_type: 'client_credentials'
      }
    };
  
    request.post(this.config.spotify.authUrl, options, cb);
  
  }

  getAuthHash () {
    return (new Buffer(this.config.spotify.clientId + ':' + this.config.spotify.clientSecret).toString('base64'));
  }

  searchArtistsByKeyword (params, token, cb) {

    let options = Object.assign(
      {},
      {
        qs: {
          q: params.keyword,
          type: 'artist',
          limit: params.limit || 20,
          offset: params.offset || 0
        }
      },
      this.getRequestOptions(token)
      );
  
    const endpoint = `${this.config.spotify.apiBase}/search`;
    request.get(this.config.spotify.apiBase + '/search', options, cb);
  }

  getArtistById (id, token, cb) {

    const endpoint = `${this.config.spotify.apiBase}/artists/${id}`;
  
    request.get(endpoint, this.getRequestOptions(token), cb);
  
  }

  getArtistAlbums (params, token, cb) {

    let options = Object.assign(
      {},
      {
        qs: {
          limit: params.limit || 20,
          offset: params.offset || 0
        }
      },
      this.getRequestOptions(token)
      );
  
    const endpoint = `${this.config.spotify.apiBase}/artists/${params.id}/albums`;
  
    request.get(endpoint, options, cb);
  
  }

  getAlbumById (id, token, cb) {

    const endpoint = `${this.config.spotify.apiBase}/albums/${id}`;
  
    request.get(endpoint, this.getRequestOptions(token), cb);
  
  }

  getRequestOptions (token) {

    return {
      json: true,
      headers: {
        'Authorization': 'Bearer ' + token
      }
    };
  
  }

}

module.exports = new Spotify(config);